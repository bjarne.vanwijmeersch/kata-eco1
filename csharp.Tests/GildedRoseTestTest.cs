// <copyright file="GildedRoseTestTest.cs">Copyright ©  2017</copyright>

using System;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using csharp;

namespace csharp.Tests
{
    [TestClass]
    [PexClass(typeof(GildedRoseTest))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    public partial class GildedRoseTestTest
    {

        [PexMethod(MaxConstraintSolverTime = 2)]
        public void TestQualityIsNeverMoreThan50([PexAssumeUnderTest] GildedRoseTest target)
        {
            target.TestQualityIsNeverMoreThan50();
            // TODO: add assertions to method GildedRoseTestTest.TestQualityIsNeverMoreThan50(GildedRoseTest)
        }
    }
}
