﻿using NUnit.Framework;
using System.Collections.Generic;

namespace csharp
{
    [TestFixture]
    public class GildedRoseTest
    {
        private IList<Item> items;
        private GildedRose app;

        [SetUp]
        public void Setup()
        {
            items = new List<Item> {
                new Item { Name = "foo", SellIn = 0, Quality = 0 },
                new Item { Name = "Aged Brie", SellIn = 5, Quality = 10 },
                new Item { Name = "Sulfuras, Hand of Ragnaros", SellIn = 0, Quality = 80 },
                new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 15, Quality = 20 },
                new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 10, Quality = 45 },
                new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 5, Quality = 47 },
                new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 0, Quality = 50 },
                new Item { Name = "Conjured Mana Cake", SellIn = 3, Quality = 6 }
            };
            app = new GildedRose(items);
        }

        [Test]
        public void TestQualityDegradesTwiceAsFastAfterSellByDate()
        {
            app.UpdateQuality(1);
            Assert.AreEqual(-1, items[0].SellIn);
            Assert.AreEqual(0, items[0].Quality);
        }

        [Test]
        public void TestQualityIsNeverNegative()
        {
            app.UpdateQuality(3);
            Assert.AreEqual(-4, items[0].SellIn);
            Assert.AreEqual(0, items[0].Quality);
        }

        [Test]
        public void TestAgedBrieIncreasesInQuality()
        {
            app.UpdateQuality(1);
            Assert.AreEqual(11, items[1].Quality);
        }

        [Test]
        public void TestQualityIsNeverMoreThan50()
        {
            app.UpdateQuality(10);
            Assert.AreEqual(50, items[1].Quality);
        }

        [Test]
        public void TestSulfurasNeverHasToBeSoldOrDecreasesInQuality()
        {
            Assert.AreEqual(0, items[2].SellIn);
            Assert.AreEqual(80, items[2].Quality);
        }

        [Test]
        public void TestBackstagePassesIncreaseInQuality()
        {
            app.UpdateQuality(1);
            Assert.AreEqual(23, items[3].Quality);
            Assert.AreEqual(46, items[4].Quality);
            Assert.AreEqual(48, items[5].Quality);
        }

        [Test]
        public void TestBackstagePassesDropToZeroAfterConcert()
        {
            app.UpdateQuality(16);
            Assert.AreEqual(0, items[3].Quality);
            Assert.AreEqual(0, items[4].Quality);
            Assert.AreEqual(0, items[5].Quality);
        }


    }
}
