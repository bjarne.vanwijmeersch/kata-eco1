﻿using ApprovalUtilities.Persistence;
using ApprovalUtilities.Persistence.EntityFramework;
using NUnit.Framework;
using NUnit.Framework.Internal.Execution;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;

namespace csharp
{
    public class GildedRose
    {
        IList<Item> Items;
        public GildedRose(IList<Item> Items)
        {
            this.Items = Items;
        }

        public void UpdateQuality(int currentDay)
        {
            foreach (var item in Items.Where(x => !(x.Name.Equals("Sulfuras, Hand of Ragnaros"))).Select(y => y).Distinct()) 
            {
                if (item.Name.Equals("Backstage passes to a TAFKAL80ETC concert"))
                {
                    CheckConcertDate(item, currentDay);
                } else if (!item.Name.Equals("Aged Brie")){
                    int mulitplier = 1;
                    if (item.Name.Contains("Conjured"))
                    {
                        mulitplier = 2;
                    } 
                    if (item.Quality > 0)
                    {
                        CheckSellByDate(item, currentDay, mulitplier);
                        CheckMinimumQuality(item);
                    }
                } else item.Quality++;
                CheckMaximumQuality(item);
            }
        }

        private void CheckMinimumQuality(Item item)
        {
            if (item.Quality < 0)
            {
                item.Quality = 0;
            }
        }

        private void CheckConcertDate(Item item, int currentDay)
        {
            int difference = item.SellIn - currentDay;

            if (difference <= 0) {
                item.Quality = 0;
            } else if (difference <= 5) {
                item.Quality += 3;
            } else if (difference <= 10) {
                item.Quality += 2;
            } else
            {
                item.Quality++;
            }
        }

        private void CheckSellByDate(Item item, int currentDay, int multiplier)
        {
            if (currentDay > item.SellIn)
            {
                item.Quality -= (2 * multiplier);
                return;
            }
            item.Quality -= (1 * multiplier);
        }

        private void CheckMaximumQuality(Item item)
        {
            if (item.Quality > 50)
            {
                item.Quality = 50;
            }
        }
    }
}
